//import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import axios from 'axios';



function App() {
  const [city, setCity]= useState("paris");
  const [temperature, setTemperature]=useState(0)
  const [loading, setLoading]=useState(false)

  function selectCity(cityName,  latitude, longitude){
    setLoading(true)
    setCity(cityName)

    axios.get('https://api.open-meteo.com/v1/forecast?latitude='+latitude+'&longitude='+longitude+'&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m')
    .then(data=>{
      const temperatureValue = data.data.current_weather.temperature;
      setTemperature(temperatureValue)
      setLoading(false)
    })
    .catch(err=>{
      console.log("error")
    })
  }

  return (
    <div className="App">
      <h1>My weather app</h1>

      <div>
      <button onClick={()=>{selectCity("Kollam",8.8932,76.6141)}}>Kollam</button>
      <button onClick={()=>{selectCity("Thiruvananthapuram",8.5241,76.9366)}}>Thiruvananthapuram</button>
      <button onClick={()=>{selectCity("Kochi",9.9312,76.2673)}}>Kochi</button>
      </div>
      
      <p>The current temperature in <span id="city">{city}</span> is <span id="temp">{temperature}°C</span></p>
    </div>
  );
}

export default App;
